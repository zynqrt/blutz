# Blutz software

Сборник программ для обработки изображений со сканера, формирования изображений для принтера и общения с "внешним миром".

# Зависимости Python > 3.5

* [pyzbar](https://pypi.org/project/pyzbar/) - декодер одномерных и двумерных кодов
* [python-barcode](https://pypi.org/project/python-barcode/) - генератор одномерных кодов
* [qrcode](https://pypi.org/project/qrcode/) - генератор QR 
* [PyQRCode](https://pypi.org/project/PyQRCode/) - другой генератор QR
* [Pillow](https://pypi.org/project/Pillow/) - работа с изображениями
* [bottle](https://pypi.org/project/bottle/) - Web фреймворк для связи с внешним миром
